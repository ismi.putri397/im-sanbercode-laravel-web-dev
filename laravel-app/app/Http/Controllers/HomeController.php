<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function tampilTable() {
        return view('layout.table');
    }

    public function tampilDataTable() {
        return view('layout.data-table');
    }
}
