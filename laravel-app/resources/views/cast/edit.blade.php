<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="/cast/{{ $cast->id }}" method="post">
        @csrf
        @method('PUT')
        <label for="">Nama</label>
        <input type="text" name="nama" value="{{ $cast->nama }}" id="">
        <br>
        <label for="">Umur</label>
        <input type="number" name="umur" value="{{ $cast->umur }}" id="">
        <br>
        <label for="">Bio</label>
        <textarea name="bio" id="" cols="30" rows="10">{{ $cast->bio }}</textarea>
        <br>
        <input type="submit" value="Update">
    </form>
</body>
</html>