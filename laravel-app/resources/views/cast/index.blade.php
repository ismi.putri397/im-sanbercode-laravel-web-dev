<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h3><a href="/cast/create">Tambah cast</a></h3>
    <table border="1">
        <thead>
            <tr>
                <td>No</td>
                <td>Nama</td>
                <td>Umur</td>
                <td>Bio</td>
                <td>Edit</td>
                <td>Detail</td>
                <td>Hapus</td>
            </tr>
        </thead>
        <tbody>
            @foreach ($casts as $key => $cast)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $cast->nama }}</td>
                <td>{{ $cast->umur }}</td>
                <td>{{ $cast->bio }}</td>
                <td><a href="/cast/{{ $cast->id }}/edit">Edit</a></td>
                <td><a href="/cast/{{ $cast->id }}">Detail</a></td>
                <td>
                    <form action="/cast/{{ $cast->id }}" method="post">
                        @csrf
                        @method("DELETE")
                        <input type="submit" value="Hapus">
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>