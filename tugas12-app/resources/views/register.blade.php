<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
 <h1>Buat Account Baru!</h1>   
 <h3>Sign Up Form</h3>
 <form action="/welcome" method="get">
    <label>First name:</label><br>
    <input type="text" name="f_name"><br><br>
    <label>Last name:</label><br>
    <input type="text" name="l_name"><br><br>
    <label>Gender:</label><br>
    <input type="radio"> Male<br>
    <input type="radio"> Female<br>
    <input type="radio"> Other<br><br>
    <label>Nationality:</label><br>
    <select name="Nationalty">
        <option value="Indonesian">Indonesian</option>
        <option value="American">American</option>
        <option value="Japan">Japan</option>
        <option value="Arabic">Arabic</option>
    </select> <br> <br> 
    <label>Language Spoken:</label><br>
    <input type="checkbox" name="Language Spoken">Bahasa Indonesia<br>
    <input type="checkbox" name="Language Spoken">English<br>
    <input type="checkbox" name="Language Spoken">Other<br><br>
    <label>Bio:</label><br>
    <textarea name="Bio" rows="10" cols="30"></textarea>
    <br><br>
    <input type="submit" name="submit" value="Sign Up">
 </form>

 
</body>
</html>